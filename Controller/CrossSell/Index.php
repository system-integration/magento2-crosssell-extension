<?php

namespace Econda\CrossSell\Controller\CrossSell;

use Magento\Catalog\Model\ProductFactory;
use \Magento\Framework\App\Action\Context;
use \Magento\Framework\App\Action\Action;
use Magento\Framework\Controller\Result\JsonFactory;
use \Magento\Framework\View\Result\PageFactory;
use \Econda\CrossSell\Helper\Data;

class Index extends Action
{
    /**
     * @var PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var ProductFactory
     */
    protected $productFactory;

    /**
     * @var JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * var Data
     */
    protected $helperData;

    /**
     * Index constructor.
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param ProductFactory $productFactory
     * @param JsonFactory $resultJsonFactory
     */
    public function __construct(Context $context, PageFactory $resultPageFactory, ProductFactory $productFactory, JsonFactory $resultJsonFactory, Data $helperData)
    {
        $this->productFactory = $productFactory;
        $this->resultPageFactory = $resultPageFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->helperData = $helperData;
        parent::__construct($context);
    }

    /**
     * @return JsonFactory
     */
    public function execute()
    {
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend(__(' heading '));

        $data = $this->getRequest()->getParams();

        $location = $data['location'];

        $data['econdaRecoParameters']['aid'] = $this->helperData->getGeneralConfig('ecinstance');

        $result = $this->sendRequestToCrossSell($data['econdaRecoParameters'], $data['currentCategory']);

        $products = $this->loadProducts($result['items'], $location, $data['econdaRecoParameters']['wid']);

        $widgetConfUncoded = explode('&', $data['data']);

        $widgetConf = [];

        for($i=0; $i < count($widgetConfUncoded); $i++) {
            $key_value = explode('=', $widgetConfUncoded[$i]);

            $widgetConf[$key_value[0]] = urldecode($key_value[1]);
        }

        if($widgetConf['ecwdidgettitle'] === 'true') {
            $widgetConf['ectitle'] = $result['widgetdetails']->title;
        }

        $block = $resultPage->getLayout()
            ->createBlock('Econda\CrossSell\Block\Index\View')
            ->setTemplate('Econda_CrossSell::view.phtml')
            ->setData('data', $widgetConf)
            ->setData('products', $products)
            ->toHtml();

        $result = $this->resultJsonFactory->create()->setData(['output' => $block]);

        return $result;
    }

    /**
     * currentyl i use sku to load products, the better way is to use the id
     *
     * @param $ids
     * @param $location
     * @param $wid
     * @return array
     */
    public function loadProducts($ids, $location, $wid)
    {
        $products = [];

        foreach ($ids as $id) {
            $product = $this->productFactory->create();
            $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $parentproduct = $objectManager->create('Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable')->getParentIdsByChild($product->getIdBySku($id->id));
            if(!empty($parentproduct)) {
                $product->load($parentproduct[0]);
            }else{
                $product->load($product->getIdBySku($id->id));
            }
            $products[$product->getId()] = $product;

            if ($location == '/') {
                $location = 'Startpage';
            }

            $emcs0 = $wid;
            $emcs1 = $location;
            $emcs2 = 'na';
            $emcs3 = $product->getId();

            $product->setData('ecdeeplink', $product->getProductUrl() .'?emcs0=' . $emcs0 . '&emcs1=' . $emcs1 . '&emcs2='. $emcs2 . '&emcs3='. $emcs3);
        }

        return $products;

    }

    public function sendRequestToCrossSell($params, $category)
    {

        $url = 'http://widgets.crosssell.info/eps/crosssell/recommendations/' . $params['aid'] . '.do?';

        foreach ($params as $key => $param) {
            $url .= $key . '=' . $param . '&';
        }

        if ($category != '') {
            $url .= 'ctxcat.ct=productcategory&ctxcat.pas=' . $category . '&ctxcat.pdl=' . urlencode('^^') . '&ctxcat.pv=default';
        }


        $url = str_replace('[', '%5B', $url);
        $url = str_replace(']', '%5D', $url);
        $url = str_replace('{', '%7B', $url);
        $url = str_replace('}', '%7D', $url);
        $url = str_replace('"', '%22', $url);

        $json = file_get_contents($url);

        return (array)json_decode($json);
    }


}
