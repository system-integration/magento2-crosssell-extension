<?php

namespace Econda\CrossSell\Block;

use \Magento\Framework\View\Element\Template\Context;
use \Magento\Store\Model\StoreManager;

class Head extends \Magento\Framework\View\Element\Template
{
    const ECONDA_CONFIG_JSSDK = 'crosssell/general/econdajssdk';
    protected $store;
    protected $scopeConfig;
    protected $storeManager;

    public function __construct( Context $context,
        StoreManager $storeManager
        )
    {
        $this->scopeConfig = $context->getScopeConfig();
        $this->storeManager = $storeManager;
        parent::__construct($context);
    }

    public function getJsSDK()
    {
        $data = '';
        $data = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA);
        if ($jsname = $this->scopeConfig->getValue(self::ECONDA_CONFIG_JSSDK, \Magento\Store\Model\ScopeInterface::SCOPE_STORE)) {
            $data .= 'econda/crosssell/'.$jsname;
        }else{
            $data = false;
        }
        return $data;
    }

}
